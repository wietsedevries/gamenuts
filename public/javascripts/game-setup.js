function _(x){
	return document.getElementById(x);
}
window.onload = function(){
  detectDevice();
}

function detectDevice() {
  if(  /Android|webOS|iPhone|iPod|BlackBerry/i.test(navigator.userAgent) ){
    _('gameLobbyMobile').style.display = "block";
  }else if(/iPad/i.test(navigator.userAgent) ){
    _('gameLobbyTablet').style.display = "block";
  }else{
    _('introScreen').style.display = "block";
		// _('gameLobbyDesktop').style.display = "block";
  }
}

;
jQuery(function($){
    'use strict';

    var IO = {

        //Connect the Socket.IO client to the Socket.IO server
        init: function() {
            IO.socket = io.connect();
            IO.bindEvents();
        },

        //Listen to events
        bindEvents : function() {
            IO.socket.on('connected', IO.onConnected );
            IO.socket.on('newGameCreated', IO.onNewGameCreated );
            IO.socket.on('playerJoinedRoom', IO.playerJoinedRoom );
            IO.socket.on('buttonC', IO.buttonC );
        },

        //The client is successfully connected!
        onConnected : function() {
            // Cache a copy of the client's socket.IO session ID on the App
            App.mySocketId = IO.socket.socket.sessionid;
            // console.log(data.message);
        },

        //A new game has been created and a random game ID has been generated.
        onNewGameCreated : function(data) {
            App.Host.gameInit(data);
            console.log('new game aangemaakt');
        },

        //A player has successfully joined the game.
        playerJoinedRoom : function(data) {
            // Update screens for both player and host
            App[App.myRole].updateWaitingScreen(data);
        },

        //Move player div on host screen
				buttonC: function (data) {
          App.Host.buttonD(data);
					console.log('button C');
        }
    };

    var App = {

        //game id
        gameId: 0,

        //This is used to differentiate between 'Host' and 'Player' browsers.
        myRole: '',   // 'Player' or 'Host'

        //socket id
        mySocketId: '',

        //This runs when the page initially loads.
        init: function () {
            App.bindEvents();

            // Initialize the fastclick library
            FastClick.attach(document.body);
        },

        //Create some click handlers for the various buttons that appear on-screen.
        bindEvents: function () {
            App.$doc = $(document);
            // Host
            App.$doc.on('click', '#createButton', App.Host.createGame);

            // Player
            App.$doc.on('click', '#joinGame',App.Player.onPlayerStartClick);
						//Phonetroller
            App.$doc.on('click', '#button1', function(){ App.Player.buttonA(1) });
						App.$doc.on('click', '#button2', function(){ App.Player.buttonA(2) });
						App.$doc.on('click', '#button3', function(){ App.Player.buttonA(3) });
						App.$doc.on('click', '#button4', function(){ App.Player.buttonA(4) });
						App.$doc.on('click', '#button5', function(){ App.Player.buttonA(5) });
						App.$doc.on('click', '#button6', function(){ App.Player.buttonA(6) });
        },

        Host : {

            //Contains references to player data
            players : [],

            car : '',

            //Keep track of the number of players that have joined the game.
            numPlayersInRoom: 0,

            // Handler for the "Start" button on the Title Screen.
            createGame: function () {
                console.log('Clicked "Create A Game"');
                IO.socket.emit('hostCreateNewGame');
            },
            //The Host screen is displayed for the first time.
            gameInit: function (data) {
                App.gameId = data.gameId;
                App.mySocketId = data.mySocketId;
                App.myRole = 'Host';
                App.Host.numPlayersInRoom = 0;

                App.Host.showDesktopLobby();
                console.log("Game started with ID: " + App.gameId + ' by host: ' + App.mySocketId);
            },

            //Show the Host screen containing the game URL and unique game ID
            showDesktopLobby : function() {
                //show desktop lobby
                $('#introScreen').hide();
                $('#gameLobbyDesktop').show();
                // Show the gameId on screen
                $('#gameCode').html(App.gameId);
            },

            //move player div on hostscreen
						buttonD: function (data) {

							switch (data.button) {
							    case 1:
											button1(data.playerId);
											console.log('Player pressed button 1');
							        break;
							    case 2:
											button2(data.playerId);
											console.log('Player pressed button 2');
							        break;
							    case 3:
											button3(data.playerId);
											console.log('Player pressed button 3');
							        break;
							    case 4:
											button4(data.playerId);
											console.log('Player pressed button 4');
							        break;
							    case 5:
											button5(data.playerId);
											console.log('Player pressed button 5');
							        break;
							    case 6:
											button6(data.playerId);
											console.log('Player pressed button 6');
							        break;
							}
						},

            //Update the Host screen when the first player joins
            updateWaitingScreen: function(data) {
                // If this is a restarted game, show the screen.
                if ( App.Host.isNewGame ) {
                    App.Host.showDesktopLobby();
                    console.log('Started a new game');
                }
                // Update host screen
                $('#playersWaiting').html( $('#playersWaiting').html()+ data.playerName +" joined the game<br/>");

                // Store the new player's data on the Host.
                App.Host.players.push(data);

                // Increment the number of players in the room
                App.Host.numPlayersInRoom += 1;
                if(App.Host.numPlayersInRoom == 1){
                  App.Host.div = 'player player1';
                }else{
                  App.Host.div = 'player player2';
                }

                $('#playerPosition').html($('#playerPosition').html() + "<div style='margin-left:0px;margin-top:0px;' class='"+App.Host.div+"' id='"+data.playerId+"'><span class='tag'>"+data.playerName+"</span></div>");
                // set amount of players
                if (App.Host.numPlayersInRoom === 2) {
                    console.log('enough players in room '+App.gameId);
                    $('#gameLobbyDesktop').hide();
                    $('#gameArea').show();



                }
            }

        },

        Player : {

            playerId : 0,
            //The player entered their name and gameId (hopefully) and clicked Start.
            onPlayerStartClick: function() {
                console.log('Player clicked "Start"');
                // Create random player id
                App.Player.playerId = "x" + Math.round( ( Math.random() * 100000 ) );
                // collect data to send to the server
                var data = {
                    gameId : +($('#inputGameId').val()),
                    playerName : $('#inputPlayerName').val() || 'anoniem',
                    playerId : App.Player.playerId
                };
                // Send the gameId, playerName and playerId to the server
                IO.socket.emit('playerJoinGame', data);
                // Set properties for the current player.
                App.myRole = 'Player';
                App.Player.myName = data.playerName;
                App.Player.id = data.playerId;
            },

            /// move left function fired onclick of button
						buttonA: function(x) {
							var data = {
                  gameId : App.gameId,
                  player : App.Player.myName,
                  playerId : App.Player.playerId,
									button : x
              };

							IO.socket.emit('buttonB', data);

							console.log('pressed : ' + x);


            },

            //Display the waiting screen for player
            updateWaitingScreen : function(data) {
                if(IO.socket.socket.sessionid === data.mySocketId){
                    App.myRole = 'Player';
                    App.gameId = data.gameId;
                    $('#gameLobbyMobile').hide();
                    $('#phonetroller').show();
                }
            }


        }

    };

    IO.init();
    App.init();

}($));
