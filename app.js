// Import the Express module
var express = require('express');

// Import the 'path' module (packaged with Node.js)
var path = require('path');
// Create a new instance of Express
var app = express();

// Import the game file.
var game = require('./game');

// Turn down the logging activity
app.use(express.logger('dev'));

// Serve static html, js, css, and image files from the 'public' directory
app.use(express.static(path.join(__dirname,'public')));
app.use(express.static(path.join(__dirname,'views')));

// Create a Node.js based http server on port 8080
var server = require('http').createServer(app).listen(3000);

// Create a Socket.IO server and attach it to the http server
var io = require('socket.io').listen(server);

// Reduce the logging output of Socket.IO
io.set('log level',1);

// Listen for Socket.IO Connections. Once connected, start the game logic.
io.sockets.on('connection', function (socket) {
    //console.log('client connected');
    game.initGame(io, socket);
});
